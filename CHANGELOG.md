## [4.5.1](https://gitlab.com/[secure]/android/compare/v4.5.0...v4.5.1) (2023-10-13)


### Bug Fixes

* update the upload logs feature to upload logs to dingding reboot ([a6f9bfa](https://gitlab.com/[secure]/android/commit/a6f9bfa93848995871baab221df54aa697220929))

# [4.5.0](https://gitlab.com/[secure]/android/compare/v4.4.1...v4.5.0) (2023-09-27)


### Features

* add the scan functions and fix some bugs ([608f260](https://gitlab.com/[secure]/android/commit/608f2605f4bc1de1a7e84f3f5af3434d43c6ac93))

## [4.4.1](https://gitlab.com/[secure]/android/compare/v4.4.0...v4.4.1) (2023-09-25)


### Bug Fixes

* add the printer features and improve the code logic which optimized printing class name ([3569956](https://gitlab.com/[secure]/android/commit/3569956d644baa291766d1d683048e83ba21a765))

# [4.4.0](https://gitlab.com/[secure]/android/compare/v4.3.2...v4.4.0) (2023-09-21)


### Features

* add the log storation and uploading to web features ([68df216](https://gitlab.com/[secure]/android/commit/68df2162da6eec864c1e280dc836c516371825d9))

## [4.3.2](https://gitlab.com/[secure]/android/compare/v4.3.1...v4.3.2) (2023-09-21)


### Bug Fixes

* prompt for modifying transaction title ([93072f0](https://gitlab.com/[secure]/android/commit/93072f00fcdf93ce6c9fabba7ed7cba4e715192e))

## [4.3.1](https://gitlab.com/[secure]/android/compare/v4.3.0...v4.3.1) (2023-09-12)


### Bug Fixes

* improve the code logic and delete some unused code ([f6278b8](https://gitlab.com/[secure]/android/commit/f6278b8da47820611d854ff5e94b6e5d8972fddd))
* modify bluetooth search image ([305b4a0](https://gitlab.com/[secure]/android/commit/305b4a03ee681daafc82df536788014f1fd928e7))
* remove ambiguity in serial connection ([ee9326c](https://gitlab.com/[secure]/android/commit/ee9326caa5b98aaab320e236cacf37a381c93353))

# [4.3.0](https://gitlab.com/[secure]/android/compare/v4.2.12...v4.3.0) (2023-08-28)


### Bug Fixes

* implement unified modification of project names with existing project file names ([b93000a](https://gitlab.com/[secure]/android/commit/b93000aaa4e77a181efe733b34606bb1083201b9))
* implement unified modification of project names with existing project file names ([a2ff052](https://gitlab.com/[secure]/android/commit/a2ff052583db957a539011db4c058912b84d5567))
* modify code according to method naming rules ([b1c0a44](https://gitlab.com/[secure]/android/commit/b1c0a444d4a80076bc4763d80152bb516031842c))
* modify files that failed compilation ([f6da7f8](https://gitlab.com/[secure]/android/commit/f6da7f8fc9efe437651b57d3edfd70674f63d532))
* modify the application package name and remove the welcome interface ([ded8caf](https://gitlab.com/[secure]/android/commit/ded8cafb619ec4c22286f3dd3442bbb0ceecbf88))
* modify the cashback amount input method ([546d8e0](https://gitlab.com/[secure]/android/commit/546d8e0581df2b7e2edd0af62d480ea2ac4af3b1))
* modify the class name to optimize the code ([17f2cd7](https://gitlab.com/[secure]/android/commit/17f2cd73c3d1ce1c5c1cdd68d856441f3f8bbf09))
* optimize the code encoding format ([a0d7646](https://gitlab.com/[secure]/android/commit/a0d764668532d869327aee38c2bf9fb671544859))
* problems during the compilation process of modifying strings files ([e727282](https://gitlab.com/[secure]/android/commit/e7272820a82630a35fda67c97779f766c3f81d49))
* problems during the compilation process of modifying strings files ([37af695](https://gitlab.com/[secure]/android/commit/37af695e09b9febd6dd97b886c90f6495c19b467))
* removed the jniLibs file to address the issue of failed introduction of so files ([1f3e482](https://gitlab.com/[secure]/android/commit/1f3e482162a2c707af9a3eca1fc7477ecae62155))


### Features

* update the android demo UI layout, and add the payment/POS info/POS update/settings/app update functions ([cdb061d](https://gitlab.com/[secure]/android/commit/cdb061d17446175f42f6b758d7da33392c66fa4d))


# [4.2.13](https://gitlab.com/[secure]/android/compare/v4.2.12...v4.3.0) (2023-08-22)


### Bug Fixes

* implement unified modification of project names with existing project file names ([b93000a](https://gitlab.com/[secure]/android/commit/b93000aaa4e77a181efe733b34606bb1083201b9))
* implement unified modification of project names with existing project file names ([a2ff052](https://gitlab.com/[secure]/android/commit/a2ff052583db957a539011db4c058912b84d5567))
* modify files that failed compilation ([f6da7f8](https://gitlab.com/[secure]/android/commit/f6da7f8fc9efe437651b57d3edfd70674f63d532))
* problems during the compilation process of modifying strings files ([e727282](https://gitlab.com/[secure]/android/commit/e7272820a82630a35fda67c97779f766c3f81d49))
* problems during the compilation process of modifying strings files ([37af695](https://gitlab.com/[secure]/android/commit/37af695e09b9febd6dd97b886c90f6495c19b467))
* removed the jniLibs file to address the issue of failed introduction of so files ([1f3e482](https://gitlab.com/[secure]/android/commit/1f3e482162a2c707af9a3eca1fc7477ecae62155))


### Features

* update the latest android demo ([cdb061d](https://gitlab.com/[secure]/android/commit/cdb061d17446175f42f6b758d7da33392c66fa4d))

## [4.2.12](https://gitlab.com/[secure]/android/compare/v4.2.11...v4.2.12) (2023-08-04)


### Bug Fixes

* update the gitlab CI CD file to skpi the second CI push ([c901e01](https://gitlab.com/[secure]/android/commit/c901e014471dd50d04fc0c193853c9e53dd03e3f))

## [4.2.11](https://gitlab.com/[secure]/android/compare/v4.2.10...v4.2.11) (2023-08-04)


### Bug Fixes

* delete unused files ([e2ddd97](https://gitlab.com/[secure]/android/commit/e2ddd9708f8c99291f35e80cd481ffccb29c97a1))

## [4.2.10](https://gitlab.com/[secure]/android/compare/v4.2.9...v4.2.10) (2023-08-01)


### Bug Fixes

* update the gitlab config for CHANGELOG and release note ([d877cf8](https://gitlab.com/[secure]/android/commit/d877cf8e046dc8fad1560f6f76217d3f6a5b077d))
